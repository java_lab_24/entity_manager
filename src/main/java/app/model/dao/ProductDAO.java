package app.model.dao;

import app.model.entity.Product;

public interface ProductDAO extends GeneralDAO<Product, String> {

}
