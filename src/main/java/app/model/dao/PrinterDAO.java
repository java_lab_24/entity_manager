package app.model.dao;

import app.model.entity.Printer;

public interface PrinterDAO extends GeneralDAO<Printer, Integer> {

}
