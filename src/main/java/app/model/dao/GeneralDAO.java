package app.model.dao;

import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

public interface GeneralDAO<T, ID> {

  List<T> findAll() throws SQLException;

  Optional<T> findById(ID id) throws SQLException;

  int create(T entity) throws SQLException;

  int update(T entity, ID oldEntityId) throws SQLException;

  int delete(ID id) throws SQLException;
}
