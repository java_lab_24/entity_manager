package app.model.dao;

import app.model.entity.Laptop;

public interface LaptopDAO extends GeneralDAO<Laptop, Integer> {

}
