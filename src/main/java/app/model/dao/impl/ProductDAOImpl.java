package app.model.dao.impl;

import app.model.dao.ProductDAO;
import app.model.entity.Product;
import app.model.transformer.Transformer;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

public class ProductDAOImpl implements ProductDAO {

  private static final String FIND_ALL = "SELECT * FROM product";
  private static final String FIND_BY_ID = "SELECT * FROM product WHERE model=?";
  private static final String CREATE = "INSERT INTO product (maker, model, type) "
      + "VALUES (?, ?, ?)";
  private static final String UPDATE = "UPDATE product "
      + "SET maker=?, model=?, type=?"
      + "WHERE model=?";
  private static final String DELETE = "DELETE FROM product WHERE model=?";

  private final Connection connection;
  private final Transformer<Product> transformer;

  public ProductDAOImpl(Connection connection) {
    this.connection = connection;
    transformer = new Transformer(Product.class);
  }

  @Override
  public List<Product> findAll() throws SQLException {
    List<Product> products = new LinkedList<>();
    Statement statement = connection.createStatement();
    ResultSet resultSet = statement.executeQuery(FIND_ALL);
    while (resultSet.next()) {
      try {
        products.add((Product) transformer.fromResultSetToEntity(resultSet));
      } catch (ClassNotFoundException e) {
        e.printStackTrace();
      }
    }
    return products;
  }

  @Override
  public Optional<Product> findById(String s) throws SQLException {
    Optional<Product> product = Optional.empty();
    PreparedStatement preparedStatement = connection.prepareStatement(FIND_BY_ID);
    preparedStatement.setString(1, s);
    ResultSet resultSet = preparedStatement.executeQuery();
    if (resultSet.next()) {
      try {
        product = Optional.of((Product) transformer.fromResultSetToEntity(resultSet));
      } catch (ClassNotFoundException e) {
        e.printStackTrace();
      }
    }
    return product;
  }

  @Override
  public int create(Product entity) throws SQLException {
    PreparedStatement preparedStatement = connection.prepareStatement(CREATE);
    preparedStatement.setString(1, entity.getMaker());
    preparedStatement.setString(2, entity.getModel());
    preparedStatement.setString(3, entity.getType());
    return preparedStatement.executeUpdate();
  }

  @Override
  public int update(Product entity, String oldEntityId) throws SQLException {
    PreparedStatement preparedStatement = connection.prepareStatement(UPDATE);
    preparedStatement.setString(1, entity.getMaker());
    preparedStatement.setString(2, entity.getModel());
    preparedStatement.setString(3, entity.getType());
    preparedStatement.setString(4, oldEntityId);
    return preparedStatement.executeUpdate();
  }

  @Override
  public int delete(String s) throws SQLException {
    PreparedStatement preparedStatement = connection.prepareStatement(DELETE);
    preparedStatement.setString(1, s);
    return preparedStatement.executeUpdate();
  }
}
