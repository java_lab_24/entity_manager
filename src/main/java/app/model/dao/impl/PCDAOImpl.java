package app.model.dao.impl;

import app.model.dao.PCDAO;
import app.model.dao.ProductDAO;
import app.model.entity.PC;
import app.model.entity.Product;
import app.model.transformer.Transformer;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

public class PCDAOImpl implements PCDAO {

  private static final String FIND_ALL = "SELECT * FROM pc";
  private static final String FIND_BY_ID = "SELECT * FROM pc WHERE code=?";
  private static final String CREATE = "INSERT INTO pc (model, speed, ram, hdd, price) "
      + "VALUES (?, ?, ?, ?, ?)";
  private static final String UPDATE = "UPDATE pc "
      + "SET model=?, speed=?, ram=?, hdd=?, price=?"
      + "WHERE code=?";
  private static final String DELETE = "DELETE FROM pc WHERE code=?";

  private final Connection connection;
  private final Transformer<PC> transformer;

  public PCDAOImpl(Connection connection) {
    this.connection = connection;
    transformer = new Transformer(PC.class);
  }

  @Override
  public List<PC> findAll() throws SQLException {
    List<PC> pcs = new LinkedList<>();
    Statement statement = connection.createStatement();
    ResultSet resultSet = statement.executeQuery(FIND_ALL);
    while (resultSet.next()) {
      try {
        pcs.add((PC) transformer.fromResultSetToEntity(resultSet));
      } catch (ClassNotFoundException e) {
        e.printStackTrace();
      }
    }
    return pcs;
  }

  @Override
  public Optional<PC> findById(Integer integer) throws SQLException {
    Optional<PC> pc = Optional.empty();
    PreparedStatement preparedStatement = connection.prepareStatement(FIND_BY_ID);
    preparedStatement.setString(1, integer.toString());
    ResultSet resultSet = preparedStatement.executeQuery();
    if (resultSet.next()) {
      try {
        pc = Optional.of((PC) transformer.fromResultSetToEntity(resultSet));
      } catch (ClassNotFoundException e) {
        e.printStackTrace();
      }
    }
    return pc;
  }

  @Override
  public int create(PC entity) throws SQLException {
    PreparedStatement preparedStatement = connection.prepareStatement(CREATE);
    preparedStatement.setString(1, entity.getModel());
    preparedStatement.setString(2, Integer.toString(entity.getSpeed()));
    preparedStatement.setString(3, Integer.toString(entity.getRam()));
    preparedStatement.setString(4, Integer.toString(entity.getHdd()));
    preparedStatement.setString(5, Float.toString(entity.getPrice()));
    ProductDAO productDAO = new ProductDAOImpl(connection);
    Product product = new Product();
    product.setMaker("A");
    product.setModel(entity.getModel());
    product.setType("PC");
    productDAO.create(product);
    return preparedStatement.executeUpdate();
  }

  @Override
  public int update(PC entity, Integer oldEntityId) throws SQLException {
    PreparedStatement preparedStatement = connection.prepareStatement(UPDATE);
    preparedStatement.setString(1, entity.getModel());
    preparedStatement.setString(2, Integer.toString(entity.getSpeed()));
    preparedStatement.setString(3, Integer.toString(entity.getRam()));
    preparedStatement.setString(4, Integer.toString(entity.getHdd()));
    preparedStatement.setString(5, Float.toString(entity.getPrice()));
    preparedStatement.setString(6, oldEntityId.toString());
    return preparedStatement.executeUpdate();
  }

  @Override
  public int delete(Integer id) throws SQLException {
    PreparedStatement preparedStatement = connection.prepareStatement(DELETE);
    preparedStatement.setString(1, id.toString());
    return preparedStatement.executeUpdate();
  }
}

