package app.model.dao.impl;

import app.model.dao.LaptopDAO;
import app.model.dao.ProductDAO;
import app.model.entity.Laptop;
import app.model.entity.Product;
import app.model.transformer.Transformer;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

public class LaptopDAOImpl implements LaptopDAO {

  private static final String FIND_ALL = "SELECT * FROM laptop";
  private static final String FIND_BY_ID = "SELECT * FROM laptop WHERE code=?";
  private static final String CREATE = "INSERT INTO laptop (model, speed, ram, hdd, price, screen) "
      + "VALUES (?, ?, ?, ?, ?, ?)";
  private static final String UPDATE = "UPDATE laptop "
      + "SET model=?, speed=?, ram=?, hdd=?, price=?, screen=?"
      + "WHERE code=?";
  private static final String DELETE = "DELETE FROM laptop WHERE code=?";

  private final Connection connection;
  private final Transformer<Laptop> transformer;

  public LaptopDAOImpl(Connection connection) {
    this.connection = connection;
    transformer = new Transformer(Laptop.class);
  }

  @Override
  public List<Laptop> findAll() throws SQLException {
    List<Laptop> laptops = new LinkedList<>();
    Statement statement = connection.createStatement();
    ResultSet resultSet = statement.executeQuery(FIND_ALL);
    while (resultSet.next()) {
      try {
        laptops.add((Laptop) transformer.fromResultSetToEntity(resultSet));
      } catch (ClassNotFoundException e) {
        e.printStackTrace();
      }
    }
    return laptops;
  }

  @Override
  public Optional<Laptop> findById(Integer integer) throws SQLException {
    Optional<Laptop> laptop = Optional.empty();
    PreparedStatement preparedStatement = connection.prepareStatement(FIND_BY_ID);
    preparedStatement.setString(1, integer.toString());
    ResultSet resultSet = preparedStatement.executeQuery();
    if (resultSet.next()) {
      try {
        laptop = Optional.of((Laptop) transformer.fromResultSetToEntity(resultSet));
      } catch (ClassNotFoundException e) {
        e.printStackTrace();
      }
    }
    return laptop;
  }

  @Override
  public int create(Laptop entity) throws SQLException {
    PreparedStatement preparedStatement = connection.prepareStatement(CREATE);
    preparedStatement.setString(1, entity.getModel());
    preparedStatement.setString(2, Integer.toString(entity.getSpeed()));
    preparedStatement.setString(3, Integer.toString(entity.getRam()));
    preparedStatement.setString(4, Integer.toString(entity.getHdd()));
    preparedStatement.setString(5, Float.toString(entity.getPrice()));
    preparedStatement.setString(6, Integer.toString(entity.getScreen()));
    ProductDAO productDAO = new ProductDAOImpl(connection);
    Product product = new Product();
    product.setMaker("B");
    product.setModel(entity.getModel());
    product.setType("Laptop");
    productDAO.create(product);
    return preparedStatement.executeUpdate();
  }

  @Override
  public int update(Laptop entity, Integer oldEntityId) throws SQLException {
    PreparedStatement preparedStatement = connection.prepareStatement(UPDATE);
    preparedStatement.setString(1, entity.getModel());
    preparedStatement.setString(2, Integer.toString(entity.getSpeed()));
    preparedStatement.setString(3, Integer.toString(entity.getRam()));
    preparedStatement.setString(4, Integer.toString(entity.getHdd()));
    preparedStatement.setString(5, Float.toString(entity.getPrice()));
    preparedStatement.setString(6, Integer.toString(entity.getScreen()));
    preparedStatement.setString(7, oldEntityId.toString());
    return preparedStatement.executeUpdate();
  }

  @Override
  public int delete(Integer id) throws SQLException {
    PreparedStatement preparedStatement = connection.prepareStatement(DELETE);
    preparedStatement.setString(1, id.toString());
    return preparedStatement.executeUpdate();
  }
}
