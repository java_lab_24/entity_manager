package app.model.dao.impl;

import app.model.dao.PrinterDAO;
import app.model.dao.ProductDAO;
import app.model.entity.Printer;
import app.model.entity.Product;
import app.model.transformer.Transformer;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

public class PrinterDAOImpl implements PrinterDAO {

  private static final String FIND_ALL = "SELECT * FROM printer";
  private static final String FIND_BY_ID = "SELECT * FROM printer WHERE code=?";
  private static final String CREATE = "INSERT INTO printer (model, color, type, price) "
      + "VALUES (?, ?, ?, ?)";
  private static final String UPDATE = "UPDATE printer "
      + "SET model=?, color=?, type=?, price=?"
      + "WHERE code=?";
  private static final String DELETE = "DELETE FROM printer WHERE code=?";

  private final Connection connection;
  private final Transformer<Printer> transformer;

  public PrinterDAOImpl(Connection connection) {
    this.connection = connection;
    transformer = new Transformer(Printer.class);
  }

  @Override
  public List<Printer> findAll() throws SQLException {
    List<Printer> printers = new LinkedList<>();
    Statement statement = connection.createStatement();
    ResultSet resultSet = statement.executeQuery(FIND_ALL);
    while (resultSet.next()) {
      try {
        printers.add((Printer) transformer.fromResultSetToEntity(resultSet));
      } catch (ClassNotFoundException e) {
        e.printStackTrace();
      }
    }
    return printers;
  }

  @Override
  public Optional<Printer> findById(Integer integer) throws SQLException {
    Optional<Printer> printer = Optional.empty();
    PreparedStatement preparedStatement = connection.prepareStatement(FIND_BY_ID);
    preparedStatement.setString(1, integer.toString());
    ResultSet resultSet = preparedStatement.executeQuery();
    if (resultSet.next()) {
      try {
        printer = Optional.of((Printer) transformer.fromResultSetToEntity(resultSet));
      } catch (ClassNotFoundException e) {
        e.printStackTrace();
      }
    }
    return printer;
  }

  @Override
  public int create(Printer entity) throws SQLException {
    PreparedStatement preparedStatement = connection.prepareStatement(CREATE);
    preparedStatement.setString(1, entity.getModel());
    preparedStatement.setString(2, Character.toString(entity.getColor()));
    preparedStatement.setString(3, entity.getType());
    preparedStatement.setString(4, Float.toString(entity.getPrice()));
    ProductDAO productDAO = new ProductDAOImpl(connection);
    Product product = new Product();
    product.setMaker("L");
    product.setModel(entity.getModel());
    product.setType("Printer");
    productDAO.create(product);
    return preparedStatement.executeUpdate();
  }

  @Override
  public int update(Printer entity, Integer oldEntityId) throws SQLException {
    PreparedStatement preparedStatement = connection.prepareStatement(UPDATE);
    preparedStatement.setString(1, entity.getModel());
    preparedStatement.setString(2, Character.toString(entity.getColor()));
    preparedStatement.setString(3, entity.getType());
    preparedStatement.setString(4, Float.toString(entity.getPrice()));
    preparedStatement.setString(5, oldEntityId.toString());
    return preparedStatement.executeUpdate();
  }

  @Override
  public int delete(Integer id) throws SQLException {
    PreparedStatement preparedStatement = connection.prepareStatement(DELETE);
    preparedStatement.setString(1, id.toString());
    return preparedStatement.executeUpdate();
  }
}

