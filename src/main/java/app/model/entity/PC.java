package app.model.entity;

import app.model.annotation.Column;
import app.model.annotation.Entity;
import app.model.annotation.PrimaryKey;

@Entity
public class PC {

  @PrimaryKey(name = "code")
  private int code;

  @Column(name = "speed")
  private int speed;

  @Column(name = "ram")
  private int ram;

  @Column(name = "hdd")
  private int hdd;

  @Column(name = "price")
  private float price;

  @Column(name = "model")
  private String model;

  public int getCode() {
    return code;
  }

  public int getSpeed() {
    return speed;
  }

  public void setSpeed(int speed) {
    this.speed = speed;
  }

  public int getRam() {
    return ram;
  }

  public void setRam(int ram) {
    this.ram = ram;
  }

  public int getHdd() {
    return hdd;
  }

  public void setHdd(int hdd) {
    this.hdd = hdd;
  }

  public float getPrice() {
    return price;
  }

  public void setPrice(float price) {
    this.price = price;
  }

  public String getModel() {
    return model;
  }

  public void setModel(String model) {
    this.model = model;
  }

  @Override
  public String toString() {
    return "PC{" +
        "code=" + code +
        ", speed=" + speed +
        ", ram=" + ram +
        ", hdd=" + hdd +
        ", price=" + price +
        ", model='" + model + '\'' +
        '}';
  }
}
