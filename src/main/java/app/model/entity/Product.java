package app.model.entity;

import app.model.annotation.Column;
import app.model.annotation.Entity;
import app.model.annotation.PrimaryKey;

@Entity
public class Product {

  @PrimaryKey(name = "model")
  private String model;

  @Column(name = "maker")
  private String maker;

  @Column(name = "type")
  private String type;

  public String getModel() {
    return model;
  }

  public void setModel(String model) {
    this.model = model;
  }

  public String getMaker() {
    return maker;
  }

  public void setMaker(String maker) {
    this.maker = maker;
  }

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  @Override
  public String toString() {
    return "Product{" +
        "model='" + model + '\'' +
        ", maker='" + maker + '\'' +
        ", type='" + type + '\'' +
        '}';
  }
}
