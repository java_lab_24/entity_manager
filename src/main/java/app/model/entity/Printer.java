package app.model.entity;

import app.model.annotation.Column;
import app.model.annotation.Entity;
import app.model.annotation.PrimaryKey;

@Entity
public class Printer {

  @PrimaryKey(name = "code")
  private int code;

  @Column(name = "color")
  private char color;

  @Column(name = "type")
  private String type;

  @Column(name = "price")
  private float price;

  @Column(name = "model")
  private String model;

  public int getCode() {
    return code;
  }

  public char getColor() {
    return color;
  }

  public void setColor(char color) {
    this.color = color;
  }

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public float getPrice() {
    return price;
  }

  public void setPrice(float price) {
    this.price = price;
  }

  public String getModel() {
    return model;
  }

  public void setModel(String model) {
    this.model = model;
  }

  @Override
  public String toString() {
    return "Printer{" +
        "code=" + code +
        ", color=" + color +
        ", type='" + type + '\'' +
        ", price=" + price +
        ", model='" + model + '\'' +
        '}';
  }
}
