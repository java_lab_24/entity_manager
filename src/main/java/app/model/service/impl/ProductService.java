package app.model.service.impl;

import app.model.dao.ProductDAO;
import app.model.entity.Product;
import app.model.service.EntityService;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

public class ProductService implements EntityService<Product, String> {

  private final ProductDAO productDAO;

  public ProductService(ProductDAO productDAO) {
    this.productDAO = productDAO;
  }

  @Override
  public boolean create(Product entity) {
    try {
      if (productDAO.create(entity) != 0) {
        return true;
      }
    } catch (SQLException e) {
      e.printStackTrace();
    }
    return false;
  }

  @Override
  public List<Product> read() {
    try {
      return productDAO.findAll();
    } catch (SQLException e) {
      e.printStackTrace();
    }
    return List.of();
  }

  @Override
  public Product read(String s) {
    Optional<Product> optionalProduct = Optional.empty();
    try {
      optionalProduct = productDAO.findById(s);
    } catch (SQLException e) {
      e.printStackTrace();
    }
    return optionalProduct.orElseThrow();
  }

  @Override
  public boolean update(Product newEntity, String oldEntityId) {
    try {
      if (productDAO.update(newEntity, oldEntityId) != 0) {
        return true;
      }
    } catch (SQLException e) {
      e.printStackTrace();
    }
    return false;
  }

  @Override
  public boolean delete(String s) {
    try {
      if (productDAO.delete(s) != 0) {
        return true;
      }
    } catch (SQLException e) {
      e.printStackTrace();
    }
    return false;
  }
}
