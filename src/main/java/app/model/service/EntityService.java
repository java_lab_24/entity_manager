package app.model.service;

import app.model.entity.Laptop;
import java.util.List;

public interface EntityService<T, ID> {

  boolean create(T entity);

  List<T> read();

  T read(ID id);

  boolean update(T newEntity, ID oldEntityId);

  boolean delete(ID id);
}
