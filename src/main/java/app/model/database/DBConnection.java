package app.model.database;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public final class DBConnection {

  private static final String PROPERTIES_SOURCE = "src\\main\\resources\\connection.properties";
  private static String url;
  private static String user;
  private static String password;
  private static Connection connection;

  static {
    getConnectionProperties();
    try {
      connection = DriverManager.getConnection(url, user, password);
    } catch (SQLException e) {
      e.printStackTrace();
    }
  }

  private static void getConnectionProperties() {
    try (InputStream input = new FileInputStream(PROPERTIES_SOURCE)) {
      Properties prop = new Properties();
      prop.load(input);
      url = prop.getProperty("url");
      user = prop.getProperty("user");
      password = prop.getProperty("password");
    } catch (IOException ex) {
      ex.printStackTrace();
    }
  }

  public static Connection getConnection() {
    return connection;
  }
}
