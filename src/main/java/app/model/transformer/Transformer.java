package app.model.transformer;

import app.model.annotation.Column;
import app.model.annotation.Entity;
import app.model.annotation.PrimaryKey;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.sql.ResultSet;
import java.sql.SQLException;

public class Transformer<T> {

  private final Class<T> clazz;
  private ResultSet resultSet;
  private Object entity;

  public Transformer(Class<T> clazz) {
    this.clazz = clazz;
  }

  public Object fromResultSetToEntity(ResultSet rs) throws SQLException, ClassNotFoundException {
    if (!clazz.isAnnotationPresent(Entity.class)) {
      throw new ClassNotFoundException("Entity annotation not found");
    }
    resultSet = rs;
    try {
      entity = clazz.getDeclaredConstructor().newInstance();
    } catch (InstantiationException | IllegalAccessException
        | InvocationTargetException | NoSuchMethodException e) {
      e.printStackTrace();
    }
    Field[] fields = clazz.getDeclaredFields();
    Field primaryKeyField;
    if ((primaryKeyField = getPrimaryKeyField(fields)) != null) {
      setPrimaryKey(primaryKeyField);
    }
    setColumns(fields);
    return entity;
  }

  private void setPrimaryKey(Field field) {
    String fieldName = field.getAnnotation(PrimaryKey.class).name();
    field.setAccessible(true);
    try {
      var value = field.getType().equals(String.class) ?
          resultSet.getString(fieldName) : Integer.parseInt(resultSet.getString(fieldName));
      field.set(entity, value);
    } catch (IllegalAccessException | SQLException e) {
      e.printStackTrace();
    }
  }

  private void setColumns(Field[] fields) {
    for (Field f : fields) {
      if (f.isAnnotationPresent(Column.class)) {
        f.setAccessible(true);
        try {
          f.set(entity, getFieldValueFromResultSet(f));
        } catch (IllegalAccessException | SQLException e) {
          e.printStackTrace();
        }
      }
    }
  }

  private Object getFieldValueFromResultSet(Field field) throws SQLException {
    Object value = null;
    Class fieldType = field.getType();
    String valueFromResultSet = resultSet.getString(field.getAnnotation(Column.class).name());
    if ((fieldType.equals(Integer.class) || (fieldType.equals(int.class)))) {
      value = Integer.parseInt(valueFromResultSet);
    } else if (fieldType.equals(Character.class) || (fieldType.equals(char.class))) {
      value = valueFromResultSet.charAt(0);
    } else if (fieldType.equals(Boolean.class) || (fieldType.equals(boolean.class))) {
      value = valueFromResultSet.equals("true");
    } else if (fieldType.equals(Float.class) || (fieldType.equals(float.class))) {
      value = Float.parseFloat(valueFromResultSet);
    } else if (fieldType.equals(Double.class) || (fieldType.equals(double.class))) {
      value = Double.parseDouble(valueFromResultSet);
    } else {
      value = valueFromResultSet;
    }
    return value;
  }

  private Field getPrimaryKeyField(Field[] fields) {
    for (Field f : fields) {
      if (f.isAnnotationPresent(PrimaryKey.class)) {
        return f;
      }
    }
    return null;
  }
}

