package app.controller;

@FunctionalInterface
public interface ControlService {

  void run();
}
