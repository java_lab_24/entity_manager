package app.controller.console;

import app.controller.ControlService;
import app.view.ConsoleView;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;
import app.controller.console.entity.*;
import org.apache.commons.collections.map.HashedMap;

public class ApplicationControlService implements ControlService {

  private final Map<String, String> mainMenu;
  private final Map<String, EntityControlService> entityControlServices;
  private final ConsoleView consoleView;
  private final Scanner scn;
  private String input = "";
  

  public ApplicationControlService(ConsoleView consoleView) {
    mainMenu = new LinkedHashMap<>();
    mainMenu.put("1", "Product");
    mainMenu.put("2", "Laptop");
    mainMenu.put("3", "PC");
    mainMenu.put("4", "Printer");
    mainMenu.put("q", "Quit");
    entityControlServices = new HashedMap();
    entityControlServices.put("1", new ProductControlService(consoleView));
    entityControlServices.put("2", new LaptopControlService(consoleView));
    entityControlServices.put("3", new PCControlService(consoleView));
    entityControlServices.put("4", new PrinterControlService(consoleView));
    this.consoleView = consoleView;
    scn = new Scanner(System.in);
  }

  @Override
  public void run() {
    consoleView.show();
    while (!input.equals("q")) {
      consoleView.printMenu(mainMenu);
      input = scn.nextLine();
      try {
        entityControlServices.get(input).run();
      } catch (Exception e) {
        System.err.println("\nNot valid input");
      }
    }
  }
}
