package app.controller.console.entity;

import app.model.dao.PrinterDAO;
import app.model.dao.impl.PrinterDAOImpl;
import app.model.entity.Printer;
import app.model.database.DBConnection;
import app.view.ConsoleView;
import java.sql.SQLException;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Optional;

public class PrinterControlService extends EntityControlService {

  private final PrinterDAO printerDAO;

  public PrinterControlService(ConsoleView consoleView) {
    super(consoleView);
    crudOperationsImplementations.put("1", this::create);
    crudOperationsImplementations.put("2", this::read);
    crudOperationsImplementations.put("3", this::update);
    crudOperationsImplementations.put("4", this::delete);
    printerDAO = new PrinterDAOImpl(DBConnection.getConnection());
  }

  @Override
  void create() {
    try {
      printerDAO.create(getNewPrinterFromUser());
      consoleView.printPrinters(printerDAO.findAll());
    } catch (Exception e) {
      System.err.println(e.getMessage());
    }
  }

  @Override
  void read() {
    System.out.println("[1] Read all");
    System.out.println("[2] Read by code");
    input = scn.nextLine();
    if (input.equals("1")) {
      try {
        consoleView.printPrinters(printerDAO.findAll());
      } catch (SQLException e) {
        e.printStackTrace();
      }
    } else if (input.equals("2")) {
      System.out.print("\nEnter code: ");
      try {
        Optional<Printer> printerOptional = printerDAO.findById(scn.nextInt());
        Printer printer = printerOptional.orElseThrow();
        consoleView.printPrinters(List.of(printer));
      } catch (Exception e) {
        e.printStackTrace();
      }
    }
  }

  @Override
  void update() {
    try {
      System.out.println("\nenter old printer code: ");
      int oldPrinterCode = scn.nextInt();
      printerDAO.update(getNewPrinterFromUser(), oldPrinterCode);
      consoleView.printPrinters(printerDAO.findAll());
    } catch (Exception e) {
      System.err.println(e.getMessage());
    }
  }

  @Override
  void delete() {
    try {
      System.out.print("\nenter printer code: ");
      int printerCode = scn.nextInt();
      printerDAO.delete(printerCode);
      consoleView.printPrinters(printerDAO.findAll());
    } catch (Exception e) {
      System.err.println(e.getMessage());
    }
  }

  private Printer getNewPrinterFromUser() throws InputMismatchException {
    Printer printer = new Printer();
    System.out.print("\nenter color (only first letter): ");
    char color = scn.nextLine().charAt(0);
    printer.setColor(color);
    System.out.print("\nenter type: ");
    String type = scn.nextLine();
    printer.setType(type);
    System.out.print("\nenter price: ");
    float price = scn.nextFloat();
    printer.setPrice(price);
    System.out.print("\nenter model: ");
    String model = scn.next();
    printer.setModel(model);
    return printer;
  }


  @Override
  public void run() {
    while (!input.equals("b")) {
      consoleView.printMenu(crudOperationsMenu);
      input = scn.nextLine();
      try {
        crudOperationsImplementations.get(input).operate();
      } catch (Exception e) {
        System.err.println("\nNot valid input");
      }
    }
  }
}
