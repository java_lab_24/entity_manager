package app.controller.console.entity;

import app.model.dao.PCDAO;
import app.model.dao.impl.PCDAOImpl;
import app.model.entity.PC;
import app.model.database.DBConnection;
import app.view.ConsoleView;
import java.sql.SQLException;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Optional;

public class PCControlService extends EntityControlService {

  private final PCDAO pcDAO;

  public PCControlService(ConsoleView consoleView) {
    super(consoleView);
    crudOperationsImplementations.put("1", this::create);
    crudOperationsImplementations.put("2", this::read);
    crudOperationsImplementations.put("3", this::update);
    crudOperationsImplementations.put("4", this::delete);
    pcDAO = new PCDAOImpl(DBConnection.getConnection());
  }

  @Override
  void create() {
    try {
      pcDAO.create(getNewPCFromUser());
      consoleView.printPCs(pcDAO.findAll());
    } catch (Exception e) {
      System.err.println(e.getMessage());
    }
  }

  @Override
  void read() {
    System.out.println("[1] Read all");
    System.out.println("[2] Read by code");
    input = scn.nextLine();
    if (input.equals("1")) {
      try {
        consoleView.printPCs(pcDAO.findAll());
      } catch (SQLException e) {
        e.printStackTrace();
      }
    } else if (input.equals("2")) {
      System.out.print("\nEnter code: ");
      try {
        Optional<PC> pcOptional = pcDAO.findById(scn.nextInt());
        PC pc = pcOptional.orElseThrow();
        consoleView.printPCs(List.of(pc));
        scn.nextLine();
      } catch (Exception e) {
        e.printStackTrace();
      }
    }
  }

  @Override
  void update() {
    try {
      System.out.println("\nenter old pc code: ");
      int oldPCCode = scn.nextInt();
      pcDAO.update(getNewPCFromUser(), oldPCCode);
      consoleView.printPCs(pcDAO.findAll());
      scn.nextLine();
    } catch (Exception e) {
      System.err.println(e.getMessage());
    }
  }

  @Override
  void delete() {
    try {
      System.out.print("\nenter pc code: ");
      int pcCode = scn.nextInt();
      pcDAO.delete(pcCode);
      consoleView.printPCs(pcDAO.findAll());
      scn.nextLine();
    } catch (Exception e) {
      System.err.println(e.getMessage());
    }
  }

  private PC getNewPCFromUser() throws InputMismatchException {
    PC pc = new PC();
    System.out.print("\nenter speed: ");
    int speed = scn.nextInt();
    pc.setSpeed(speed);
    System.out.print("\nenter ram: ");
    int ram = scn.nextInt();
    pc.setRam(ram);
    System.out.print("\nenter hdd: ");
    int hdd = scn.nextInt();
    pc.setHdd(hdd);
    System.out.print("\nenter price: ");
    float price = scn.nextFloat();
    pc.setPrice(price);
    System.out.print("\nenter model: ");
    scn.nextLine();
    String model = scn.next();
    pc.setModel(model);
    return pc;
  }


  @Override
  public void run() {
    while (!input.equals("b")) {
      consoleView.printMenu(crudOperationsMenu);
      input = scn.nextLine();
      try {
        crudOperationsImplementations.get(input).operate();
      } catch (Exception e) {
        System.err.println("\nNot valid input");
      }
    }
  }
}
