package app.controller.console.entity;

import app.controller.ControlService;
import app.view.ConsoleView;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;
import org.apache.commons.collections.map.HashedMap;

public abstract class EntityControlService implements ControlService {

  final Map<String, String> crudOperationsMenu;
  Map<String, CrudOperation> crudOperationsImplementations;
  final ConsoleView consoleView;
  final Scanner scn;
  String input = "";

  public EntityControlService(ConsoleView consoleView) {
    crudOperationsMenu = new LinkedHashMap<>();
    crudOperationsMenu.put("1", "Create");
    crudOperationsMenu.put("2", "Read");
    crudOperationsMenu.put("3", "Update");
    crudOperationsMenu.put("4", "Delete");
    crudOperationsMenu.put("b", "Back to Main Menu");
    crudOperationsImplementations = new HashedMap();
    this.consoleView = consoleView;
    scn = new Scanner(System.in);
  }

  abstract void create();

  abstract void read();

  abstract void update();

  abstract void delete();
}
