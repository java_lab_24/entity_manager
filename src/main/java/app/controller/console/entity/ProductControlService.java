package app.controller.console.entity;

import app.model.dao.ProductDAO;
import app.model.dao.impl.ProductDAOImpl;
import app.model.entity.Product;
import app.model.database.DBConnection;
import app.view.ConsoleView;
import java.sql.SQLException;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Optional;

public class ProductControlService extends EntityControlService {

  private final ProductDAO productDAO;

  public ProductControlService(ConsoleView consoleView) {
    super(consoleView);
    crudOperationsImplementations.put("1", this::create);
    crudOperationsImplementations.put("2", this::read);
    crudOperationsImplementations.put("3", this::update);
    crudOperationsImplementations.put("4", this::delete);
    productDAO = new ProductDAOImpl(DBConnection.getConnection());
  }

  @Override
  void create() {
    try {
      productDAO.create(getNewProductFromUser());
      consoleView.printProducts(productDAO.findAll());
    } catch (Exception e) {
      System.err.println(e.getMessage());
    }
  }

  @Override
  void read() {
    System.out.println("[1] Read all");
    System.out.println("[2] Read by code");
    input = scn.nextLine();
    if (input.equals("1")) {
      try {
        consoleView.printProducts(productDAO.findAll());
      } catch (SQLException e) {
        e.printStackTrace();
      }
    } else if (input.equals("2")) {
      System.out.print("\nEnter code: ");
      try {
        Optional<Product> productOptional = productDAO.findById(scn.nextLine());
        Product product = productOptional.orElseThrow();
        consoleView.printProducts(List.of(product));
      } catch (Exception e) {
        e.printStackTrace();
      }
    }
  }

  @Override
  void update() {
    try {
      System.out.println("\nenter old product model: ");
      String oldProductModel = scn.nextLine();
      productDAO.update(getNewProductFromUser(), oldProductModel);
      consoleView.printProducts(productDAO.findAll());
    } catch (Exception e) {
      System.err.println(e.getMessage());
    }
  }

  @Override
  void delete() {
    try {
      System.out.print("\nenter product code: ");
      String productModel = scn.nextLine();
      productDAO.delete(productModel);
      consoleView.printProducts(productDAO.findAll());
    } catch (Exception e) {
      System.err.println(e.getMessage());
    }
  }

  private Product getNewProductFromUser() throws InputMismatchException {
    Product product = new Product();
    System.out.print("\nenter maker: ");
    String maker = scn.nextLine();
    product.setMaker(maker);
    System.out.print("\nenter type: ");
    String type = scn.nextLine();
    product.setType(type);
    System.out.print("\nenter model: ");
    String model = scn.next();
    product.setModel(model);
    return product;
  }


  @Override
  public void run() {
    while (!input.equals("b")) {
      consoleView.printMenu(crudOperationsMenu);
      input = scn.nextLine();
      try {
        crudOperationsImplementations.get(input).operate();
      } catch (Exception e) {
        System.err.println("\nNot valid input");
      }
    }
  }
}
