package app.controller.console.entity;

import app.model.dao.LaptopDAO;
import app.model.dao.impl.LaptopDAOImpl;
import app.model.entity.Laptop;
import app.model.database.DBConnection;
import app.view.ConsoleView;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

public class LaptopControlService extends EntityControlService {

  private final LaptopDAO laptopDAO;

  public LaptopControlService(ConsoleView consoleView) {
    super(consoleView);
    crudOperationsImplementations.put("1", this::create);
    crudOperationsImplementations.put("2", this::read);
    crudOperationsImplementations.put("3", this::update);
    crudOperationsImplementations.put("4", this::delete);
    laptopDAO = new LaptopDAOImpl(DBConnection.getConnection());
  }

  @Override
  void create() {
    try {
      laptopDAO.create(getNewLaptopFromUser());
      consoleView.printLaptops(laptopDAO.findAll());
    } catch (Exception e) {
      System.err.println(e.getMessage());
    }
  }

  @Override
  void read() {
    System.out.println("[1] Read all");
    System.out.println("[2] Read by code");
    input = scn.nextLine();
    if (input.equals("1")) {
      try {
        consoleView.printLaptops(laptopDAO.findAll());
      } catch (SQLException e) {
        e.printStackTrace();
      }
    } else if (input.equals("2")) {
      System.out.print("\nEnter code: ");
      try {
        Optional<Laptop> laptopOptional = laptopDAO.findById(Integer.parseInt(scn.nextLine()));
        Laptop laptop = laptopOptional.orElseThrow();
        consoleView.printLaptops(List.of(laptop));
      } catch (Exception e) {
        e.printStackTrace();
      }
    }
  }

  @Override
  void update() {
    try {
      System.out.println("\nenter old laptop code: ");
      int oldLaptopCode = Integer.parseInt(scn.nextLine());
      laptopDAO.update(getNewLaptopFromUser(), oldLaptopCode);
      consoleView.printLaptops(laptopDAO.findAll());
    } catch (Exception e) {
      System.err.println(e.getMessage());
    }
  }

  @Override
  void delete() {
    try {
      System.out.print("\nenter laptop code: ");
      int laptopCode = Integer.parseInt(scn.nextLine());
      laptopDAO.delete(laptopCode);
      consoleView.printLaptops(laptopDAO.findAll());
    } catch (Exception e) {
      System.err.println(e.getMessage());
    }
  }

  private Laptop getNewLaptopFromUser() throws NumberFormatException {
    Laptop laptop = new Laptop();
    System.out.print("\nenter speed: ");
    int speed = Integer.parseInt(scn.nextLine());
    laptop.setSpeed(speed);
    System.out.print("\nenter ram: ");
    int ram = Integer.parseInt(scn.nextLine());
    laptop.setRam(ram);
    System.out.print("\nenter hdd: ");
    int hdd = Integer.parseInt(scn.nextLine());
    laptop.setHdd(hdd);
    System.out.print("\nenter price: ");
    float price = Float.parseFloat(scn.nextLine());
    laptop.setPrice(price);
    System.out.print("\nenter screen: ");
    int screen = Integer.parseInt(scn.nextLine());
    laptop.setScreen(screen);
    System.out.print("\nenter model: ");
    String model = scn.nextLine();
    laptop.setModel(model);
    return laptop;
  }


  @Override
  public void run() {
    while (!input.equals("b")) {
      consoleView.printMenu(crudOperationsMenu);
      input = scn.nextLine();
      try {
        crudOperationsImplementations.get(input).operate();
      } catch (Exception e) {
        System.err.println("\nNot valid input");
      }
    }
  }
}
