package app.controller.console.entity;

interface CrudOperation {

  void operate();
}
