package app;

import app.controller.console.ApplicationControlService;
import app.controller.ControlService;
import app.view.ConsoleView;
import app.view.impl.ConsoleViewImpl;

public class Application {

  private static final ConsoleView view = new ConsoleViewImpl();
  private static final ControlService applicationControlService = new ApplicationControlService(view);

  public static void main(String[] args) {
    applicationControlService.run();
  }
}
