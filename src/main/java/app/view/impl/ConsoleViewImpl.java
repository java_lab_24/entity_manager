package app.view.impl;

import app.model.entity.Laptop;
import app.model.entity.PC;
import app.model.entity.Printer;
import app.model.entity.Product;
import app.view.ConsoleView;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

public class ConsoleViewImpl implements ConsoleView {
  
  @Override
  public void show() {
    System.out.println(APP_TITLE);
  }

  @Override
  public void printMenu(Map<String, String> menu) {
    System.out.println();
    for (Entry<String, String> entry : menu.entrySet()) {
      System.out.print("[" + entry.getKey() + "] " + entry.getValue() + "\n");
    }
  }

  @Override
  public void printLaptops(List<Laptop> laptops) {
    System.out.println("\n----------------------------------------------------------------------------");
    System.out.printf("%4s %5s %5s %5s %10s %10s %10s", "code", "speed", "ram", "hdd", "price", "screen", "model");
    System.out.println();
    System.out.println("----------------------------------------------------------------------------");
    for(Laptop laptop: laptops){
      System.out.format("%4s %5d %5d %5d %10.2f %10d %10s",
          laptop.getCode(), laptop.getSpeed(), laptop.getRam(), laptop.getHdd(), laptop.getPrice(), laptop.getScreen(), laptop.getModel());
      System.out.println();
    }
    System.out.println("----------------------------------------------------------------------------");
  }

  @Override
  public void printPCs(List<PC> pcs) {
    System.out.println("\n----------------------------------------------------------------------------");
    System.out.printf("%4s %5s %5s %5s %10s %10s", "code", "speed", "ram", "hdd", "price", "model");
    System.out.println();
    System.out.println("----------------------------------------------------------------------------");
    for(PC pc: pcs){
      System.out.format("%4s %5d %5d %5d %10.2f %10s",
          pc.getCode(), pc.getSpeed(), pc.getRam(), pc.getHdd(), pc.getPrice(), pc.getModel());
      System.out.println();
    }
    System.out.println("----------------------------------------------------------------------------");
  }

  @Override
  public void printPrinters(List<Printer> printers) {
    System.out.println("\n----------------------------------------------------------------------------");
    System.out.printf("%4s %5s %5s %5s %10s", "code", "color", "type", "price", "model");
    System.out.println();
    System.out.println("----------------------------------------------------------------------------");
    for(Printer printer: printers){
      System.out.format("%4s %5c %5s %10.2f %5s",
          printer.getCode(), printer.getColor(), printer.getType(), printer.getPrice(), printer.getModel());
      System.out.println();
    }
    System.out.println("----------------------------------------------------------------------------");
  }

  @Override
  public void printProducts(List<Product> products) {
    System.out.println("----------------------------------------------------------------------------");
    System.out.printf("%4s %5s %5s", "maker", "type", "model");
    System.out.println();
    System.out.println("----------------------------------------------------------------------------");
    for(Product product: products){
      System.out.format("%4s %5s %5s",
           product.getMaker(), product.getType(), product.getModel());
      System.out.println();
    }
    System.out.println("----------------------------------------------------------------------------");
  }
}
