package app.view;

public interface View {

  String APP_TITLE = "Entity Manager";

  void show();
}
