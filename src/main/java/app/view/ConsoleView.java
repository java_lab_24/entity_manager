package app.view;

import app.model.entity.Laptop;
import app.model.entity.PC;
import app.model.entity.Printer;
import app.model.entity.Product;
import java.util.List;
import java.util.Map;

public interface ConsoleView extends View {

  void printMenu(Map<String, String> menu);

  void printLaptops(List<Laptop> laptops);

  void printPCs(List<PC> pcs);

  void printPrinters(List<Printer> printers);

  void printProducts(List<Product> products);
}
