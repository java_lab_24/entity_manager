package app.model.service;

import app.model.entity.Product;

public class ProductFake {

  static final Product PRODUCT = new Product();

  static {
    PRODUCT.setModel("1");
    PRODUCT.setType("fake");
    PRODUCT.setMaker("fake");
  }
}
