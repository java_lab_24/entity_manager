package app.model.service;

import app.model.service.impl.ProductService;
import org.junit.Assert;
import org.junit.Test;

public class ProductServiceTest {

  private ProductService productService;

  public ProductServiceTest() {
    productService = new ProductService(new ProductDAOFake());
  }

  @Test
  public void create() {
    Assert.assertTrue(productService.create(ProductFake.PRODUCT));
  }

  @Test
  public void readAllFromNotEmptyDB() {
    Assert.assertTrue(productService.read().size() > 0);
  }

  @Test
  public void readByIdExistRecord() {
    Assert.assertTrue(productService.read(ProductFake.PRODUCT.getModel()) != null);
  }

  @Test
  public void updateExistRecord() {
    Assert.assertTrue(productService.update(ProductFake.PRODUCT, ProductFake.PRODUCT.getModel()));
  }

  @Test
  public void deleteExistRecord() {
    Assert.assertTrue(productService.update(ProductFake.PRODUCT, ProductFake.PRODUCT.getModel()));
  }
}
