package app.model.service;

import app.model.dao.ProductDAO;
import app.model.entity.Product;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

class ProductDAOFake implements ProductDAO {

  private final List<Product> products;

  ProductDAOFake() {
    products = new LinkedList<>();
    products.add(ProductFake.PRODUCT);
  }


  @Override
  public List<Product> findAll() throws SQLException {
    return products;
  }

  @Override
  public Optional<Product> findById(String s) throws SQLException {
    return Optional.of(ProductFake.PRODUCT);
  }

  @Override
  public int create(Product entity) throws SQLException {
    return 1;
  }

  @Override
  public int update(Product entity, String oldEntityId) throws SQLException {
    return 1;
  }

  @Override
  public int delete(String s) throws SQLException {
    return 1;
  }
}